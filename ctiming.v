`timescale 1ns / 1ns

module ctiming(input clk
,input permit
,input fault
,output rf_master
,input [0:0] rf_go
,input [15:0] fault_delay
,output [4:0] status
);

// Master RF status flip-flops
reg rf_master_r=0;
wire turn_off = ~permit | fault;
wire fault1 = rf_master_r & turn_off;  // single cycle
reg rf_go_d=0;
always @(posedge clk) begin
	rf_go_d<=rf_go;
	rf_master_r <= turn_off ? 1'b0 : (rf_go&~rf_go_d) ? 1'b1 : rf_master_r;
end
assign rf_master = rf_master_r;
assign status={turn_off,rf_master,permit,fault,rf_go};
// Non-retriggerable one-shot to delay fault signal
reg [15:0] fault_count=0;
reg fault_trig_r=0;
wire fc_zero=~(|fault_count);
reg fc_zero_r=0;
always @(posedge clk) begin
	if (fault1|~fc_zero)
		fault_count <= fc_zero ? fault_delay : fault_count-1'b1;
	fc_zero_r <= fc_zero;
	fault_trig_r <= fc_zero & ~fc_zero_r;
end
assign fault_trig = fault_trig_r;

endmodule
