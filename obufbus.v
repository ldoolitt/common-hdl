module obufbus #(parameter WIDTH=4)(input [WIDTH-1:0] I,output [WIDTH-1:0] O);
genvar i;
generate for (i=0;i<WIDTH;i=i+1) begin
	OBUF ibuf(.I(I[i]),.O(O[i]));
end
endgenerate
endmodule
