interface ifiic();
wire scl;
wire sda;
modport hw(input scl
,inout sda);
endinterface
