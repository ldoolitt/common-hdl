module samefreqxdomain#(
parameter DW=16
)(input clkw
,input clkr
,input [DW-1:0] dataw
,output [DW-1:0] datar
,input [0:0] reset
,output validr
);
reg [DW-1:0] buf4[3:0];
reg [3:0] written=0;
reg [1:0] waddr=0;
reg [1:0] raddr=2'h2;
reg [DW-1:0] datar_r=0;
reg validr_r=0;

always @(posedge clkw) begin
	if (reset) begin
		waddr<=2'b0;
		written<=4'b0;
	end
	else begin
		waddr<=waddr+2'b1;
		written[waddr]<=1'b1;
	end
	buf4[waddr]<=dataw;
end
always @(posedge clkr) begin
	if (reset) begin
		raddr<=2'h2;
		validr_r<=1'b0;
	end
	else begin
		raddr<=raddr+2'b1;
		validr_r<=written[raddr];
	end
	datar_r<=buf4[raddr];
end
assign datar=datar_r;
assign validr=validr_r;
endmodule
