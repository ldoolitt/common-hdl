`include "constants.vams"
`timescale 1ns / 10ps
module cmultiplier_tb();
reg dspclk;
integer cc;
initial begin
	if ($test$plusargs("fst")) begin
		$dumpfile("cmultiplier.fst");
		$dumpvars(7,cmultiplier_tb);
	end
	//  $dumpfile("hires.fst");
	//  $dumpvars(6,hires_tb);
	for (cc=0; cc<30000; cc=cc+1) begin
		dspclk=0; #5;
		dspclk=1; #5;
	end
	$finish();
end
wire  rst=cc<100;
localparam XWIDTH=18;
localparam YWIDTH=18;
localparam ZWIDTH=48;
wire signed [XWIDTH-1:0] xi=cc>1000 ? cc>2000 ? 3000 : 5000 : -2000;
wire signed [XWIDTH-1:0] xr=cc>1000 ? cc>2000 ? 3000 : 5000 : -2000;
wire signed [YWIDTH-1:0] yi=cc>1000 ? cc>2000 ? 3000 : 5000 : -2000;
wire signed [YWIDTH-1:0] yr=cc>1000 ? cc>2000 ? 3000 : 5000 : -2000;
wire signed [ZWIDTH-1:0] zr;
wire signed [ZWIDTH-1:0] zi;
cmultiplier #(.FPGA("SPARTAN7"))
cmultiplier(.clk(dspclk),.rst(rst),.xi(xi),.xr(xr),.yi(yi),.yr(yr),.zr(zr),.zi(zi));
cmultiplier #(.FPGA("SPARTAN6"))
cmultipliers6(.clk(dspclk),.rst(rst),.xi(xi),.xr(xr),.yi(yi),.yr(yr));
cmultiplier #(.FPGA("KINTEX7"))
cmultiplierk7(.clk(dspclk),.rst(rst),.xi(xi),.xr(xr),.yi(yi),.yr(yr));
endmodule
