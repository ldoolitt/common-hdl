`timescale 1ns / 1ns

// Feedback path gain module for warm-cavity LLRF control
// Larry Doolittle, LBNL
//
// P and I feedback, each with programmable gain
// Combined, they give a control-system zero to compensate for the cavity pole
// See http://recycle.lbl.gov/~ldoolitt/llrf/neariq.pdf, except I have a fully
// controllable phase/amplitude gain block instead of "B", and this bandpass
// filter is messier (see cset3.m for values of c-1 and d to use).
// Also see phshift_tb.v for an example of computing alpha and beta.

module fdbk_gain(
	input clk,
	input signed [15:0] d_in,
	input zerome,
	output signed [15:0] d_out
	// Local bus
/*
	input [16:0] lb_data,
	input [6:0] lb_addr,
	input lb_write
*/
	,input [15:0] gainp_alpha
	,input [15:0] gainp_beta
);

// Host-settable registers
/*`include "regmap_app.vh"
`REG_gainp_alpha  // proportional gain alpha
`REG_gainp_beta   // proportional gain beta
*/
`ifdef PERFECT_IIR
`REG_gaini_alpha  // B gain alpha
`REG_gaini_beta   // B gain beta
`REG_band_d       // bandpass filter coefficient d
`REG_band_cm1     // bandpass filter coefficient c-1
`endif

// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})

wire signed [15:0] d1;
phshift inshift(.clk(clk), .d_in(d_in), .d_out(d1),
	.gain1(gainp_alpha), .gain2(gainp_beta));

`ifdef PERFECT_IIR  // never happens
wire signed [15:0] d2;
phshift bpshift(.clk(clk), .d_in(d1), .d_out(d2),
	.gain1(gaini_alpha), .gain2(gaini_beta));

wire signed [17:0] d3;
bandpass3 pole(.clk(clk), .inp(d2), .zerome(zerome), .oe(1'b1), .out(d3),
	.cm1(band_cm1), .d(band_d));

reg signed [15:0] d4=0;
wire signed [16:0] d4sum = d1+$signed(d3[17:2]);
always @(posedge clk) d4 <=`SAT(d4sum, 16, 15);

assign d_out = d4;
`else
// Need some other path to provide the integral term.
// (see ifdbk_gain.v)
assign d_out = d1;
`endif

endmodule
