`timescale 1ns / 100ps
module qubic_tb();
reg sysclk=0;
integer cc=0;
initial begin
	$dumpfile("chainreset.vcd");
	$dumpvars(17,qubic_tb);
	for (cc=0; cc<10000; cc=cc+1) begin
//	while (1) begin
		cc=cc+1;

		sysclk=0; #2.5;
		sysclk=1; #2.5;
	end
	$finish();
end
reg [31:0] clkcnt2=0;
wire [30:0] clkcnt=clkcnt2[31:1];
always @(sysclk) begin
	clkcnt2<=clkcnt2+1;
end

localparam NSTEP=3;

// chainreset cross wire 
wire [NSTEP-1:0] done;
wire [NSTEP-1:0] donestrobe;
wire [NSTEP-1:0] error;
wire [NSTEP-1:0] resetout;
wire [NSTEP-1:0] donecriteria;//={clkcnt>=2500,clkcnt>=1200,clkcnt>=700};
wire [NSTEP*16-1:0] readylength;//={16'd20,16'd10,16'd1};
wire [NSTEP*16-1:0] resettodonecheck;//={16'd10,16'd20,16'd1};
wire [NSTEP*16-1:0] resetlength;//={16'd200,16'd1,16'd10};
wire [NSTEP*32-1:0] resettimeout;//={32'd2800,32'd1500,32'd1000};

wire reset0,reset1,reset2;
localparam RESETMMCM=0;assign donecriteria[RESETMMCM]=clkcnt>=700; assign reset0=resetout[RESETMMCM];assign resetlength[RESETMMCM*16+:16]=16'h1;assign readylength[RESETMMCM*16+:16]=16'd5; assign resettodonecheck[RESETMMCM*16+:16]=16'd8;assign resettimeout[RESETMMCM*32+:32]=32'h5000;
localparam RESETTEST=1;assign donecriteria[RESETTEST]=clkcnt>=1200; assign reset2=resetout[RESETTEST];assign resetlength[RESETTEST*16+:16]=16'h30;assign readylength[RESETTEST*16+:16]=16'd5; assign resettodonecheck[RESETTEST*16+:16]=16'd8;assign resettimeout[RESETTEST*32+:32]=32'h50;
localparam RESETIDELAY=2;assign donecriteria[RESETIDELAY]=clkcnt>=2500; assign reset1=resetout[RESETIDELAY];assign resetlength[RESETIDELAY*16+:16]=16'h100;assign readylength[RESETIDELAY*16+:16]=16'd5; assign resettodonecheck[RESETIDELAY*16+:16]=16'd8;assign resettimeout[RESETIDELAY*32+:32]=32'h1000;


wire resetin=clkcnt==100;
wire stbdone;
chainreset #(.NSTEP(NSTEP))
chainreset(.clk(sysclk)
,.resetin(resetin|(stbdone & ~&done))
,.resetout(resetout)
,.donecriteria(donecriteria)
,.resetlength(resetlength)
,.readylength(readylength)
,.resettodonecheck(resettodonecheck)
,.resettimeout(resettimeout)
,.done(done)
,.stbdone(stbdone)
);
endmodule
