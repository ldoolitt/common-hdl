// cordic with x/y/p/op can be changed every cycle, with option to add input buffer, output normalize
`timescale 1ns / 1ns
module cordicg(clk, opin, xin, yin, phasein, xout, yout, phaseout,error,gin,gout);
parameter WIDTH=19;
parameter NSTAGE=19;
parameter NORMALIZE=0;
localparam ZWIDTH=NSTAGE+1;
parameter BUFIN=0;
parameter GW=1;
input clk;
input [0:0] opin;  //  1 forces y to zero (rect to polar), 0 forces theta to zero (polar to rect)
input signed [WIDTH-1:0] xin;
input signed [WIDTH-1:0] yin;
input [ZWIDTH-1:0] phasein;
output signed [WIDTH-1:0] xout;
output signed [WIDTH-1:0] yout;
output [ZWIDTH-1:0] phaseout;
output error;
input [GW-1:0] gin;
output [GW-1:0] gout;

localparam [33*32-1:0] PHASE_CONST ={32'd0,32'd0,32'd1,32'd1,32'd3,32'd5,32'd10,32'd20,32'd41,32'd81,32'd163,32'd326,32'd652,32'd1304,32'd2608,32'd5215,32'd10430,32'd20861,32'd41722,32'd83443,32'd166886,32'd333772,32'd667544,32'd1335087,32'd2670163,32'd5340245,32'd10679838,32'd21354465,32'd42667331,32'd85004756,32'd167458907,32'd316933406,32'd536870912
};// print ','.join(["%d'd%d"%(32,int(i)) for i in floor(arctan(1.0/(2**arange(32,-1,-1)))*2/2/pi*2**(32-1)+0.5)]). I guess this is the highest zwidth we should use, if not, we can generate something even bigger, and adjust ain location accordingly

wire [31:0] PHASE [0:32];
generate 
for (genvar i =0;i<=32;i=i+1) begin
	assign PHASE[i]=PHASE_CONST[i*32+31:i*32];
end
endgenerate
wire [31:0] phase0=PHASE[0];
wire [31:0] phase1=PHASE[1];
wire [31:0] phase2=PHASE[2];
wire [31:0] phase3=PHASE[3];
wire [31:0] phase4=PHASE[4];
wire [31:0] phase5=PHASE[5];
wire [31:0] phase6=PHASE[6];
wire [31:0] phase7=PHASE[7];

reg signed [WIDTH-1:0] xout_r=0;
reg signed [WIDTH-1:0] yout_r=0;
reg [ZWIDTH-1:0] phaseout_r=0;
reg errorr=0;
wire signed [WIDTH-1:0] xin_1;
wire signed [WIDTH-1:0] yin_1;
wire [ZWIDTH-1:0] phasein_1;
wire opin_1;
assign error=|errorr;
localparam WIDTHP1=WIDTH+1;
generate
if (BUFIN==0) begin
	assign xin_1=xin;
	assign yin_1=yin;
	assign phasein_1=phasein;
	assign opin_1=opin;
end
else begin
	reg signed [WIDTH-1:0] xin_d=0;
	reg signed [WIDTH-1:0] yin_d=0;
	reg [ZWIDTH-1:0] phasein_d=0;
	reg opin_d=0;
	always @(posedge clk) begin
		xin_d<=xin;
		yin_d<=yin;
		phasein_d<=phasein;
		opin_d<=opin;
	end
	assign xin_1=xin_d;
	assign yin_1=yin_d;
	assign phasein_1=phasein_d;
	assign opin_1=opin_d;
end
endgenerate
wire shiftpi=(opin_1&xin_1[WIDTH-1])|((~opin_1)&(phasein_1[ZWIDTH-1]^phasein_1[ZWIDTH-2]));
wire plusall [0:NSTAGE] ;
reg op [0:NSTAGE];
reg pluscheck [0:NSTAGE];
reg pluscheck0 [0:NSTAGE];
reg signed [WIDTHP1-1:0] x[0:NSTAGE];
reg signed [WIDTHP1-1:0] y[0:NSTAGE];
reg [ZWIDTH-1:0] z[0:NSTAGE];
reg signed [WIDTHP1-1:0] xshift[0:NSTAGE];
reg signed [WIDTHP1-1:0] yshift[0:NSTAGE];
reg [ZWIDTH-1:0] zshift[0:NSTAGE];
always @(posedge clk) begin
	x[0] <= shiftpi ? -((WIDTHP1)'(signed'(xin_1))) : (WIDTHP1)'(signed'(xin_1));
	y[0] <= shiftpi ? -((WIDTHP1)'(signed'(yin_1))) : (WIDTHP1)'(signed'(yin_1));
	z[0] <= shiftpi ? {{1{~phasein_1[ZWIDTH-1]}},phasein_1[ZWIDTH-2:0]} : {phasein_1[ZWIDTH-1],phasein_1[ZWIDTH-2:0]};
	op[0]<=opin_1;
end
genvar istage;
generate 
for (istage=0; istage<NSTAGE; istage=istage+1) begin: stage
	wire oping = op[istage];
//	wire signed[WIDTHP1-1:0] xinp =(x[istage]);
//	wire signed[WIDTHP1-1:0] yinp =(y[istage]);
//	wire [ZWIDTH-1:0] zin =z[istage];
	wire [ZWIDTH-1:0] ain=PHASE[istage][31:32-ZWIDTH];
	assign plusall[istage]=oping ? ~y[istage][WIDTHP1-1] : z[istage][ZWIDTH-1];
	wire plus=oping ? ~y[istage][WIDTHP1-1] : z[istage][ZWIDTH-1];
	wire x_1;
	wire y_1;
	if (istage>=1) begin
		assign x_1 = (&x[istage][WIDTHP1-1:istage-1]) | (~|(x[istage][WIDTHP1-1:istage-1]));
		assign y_1 = (&y[istage][WIDTHP1-1:istage-1]) | (~|(y[istage][WIDTHP1-1:istage-1]));
	end
	else begin
		assign x_1=0;
		assign y_1=0;
	end
	//		wire [WIDTHP1-1:0] xshift =  x_1 & (istage>0) ? 0 : xinp>>>istage;  // not accumating -1 because -1>>>1 = -1
	//		wire [WIDTHP1-1:0] yshift =  y_1 & (istage>0) ? 0 : yinp>>>istage;
	//		wire [ZWIDTH-1:0]  zshift = ain;

	always @(negedge clk) begin
		xshift[istage] <=  x_1 & (istage>0) ? 0 : x[istage]>>>istage;  // not accumating -1 because -1>>>1 = -1
		yshift[istage] <=  y_1 & (istage>0) ? 0 : y[istage]>>>istage;
		zshift[istage] <= ain;
	end

	always @(posedge clk) begin
		x[istage+1]<= plusall[istage] ? (x[istage] + yshift[istage]) : (x[istage] - yshift[istage]) ;
		y[istage+1]<= plusall[istage] ? (y[istage] - xshift[istage]) : (y[istage] + xshift[istage]);
		z[istage+1]<= plusall[istage] ? z[istage] + zshift[istage] : z[istage] - zshift[istage];
		op[istage+1]<= oping;
		pluscheck[istage+1]<=plusall[istage];
		pluscheck0[istage+1]<=plusall[istage];
	end
end

wire signed [WIDTHP1-1:0] xg=x[NSTAGE-1];
wire signed [WIDTHP1-1:0] yg=y[NSTAGE-1];
wire         [ZWIDTH-1:0] zg=z[NSTAGE-1];

reg signed [WIDTH:0] xsum0=0;
reg signed [WIDTH:0] xsum1=0;
reg signed [WIDTH:0] xsum2=0;
reg signed [WIDTH:0] xsum2_d=0;
reg signed [WIDTH:0] xsum3=0;
reg signed [WIDTH:0] ysum0=0;
reg signed [WIDTH:0] ysum1=0;
reg signed [WIDTH:0] ysum2=0;
reg signed [WIDTH:0] ysum2_d=0;
reg signed [WIDTH:0] ysum3=0;
reg [ZWIDTH-1:0] zg0=0;
reg [ZWIDTH-1:0] zg1=0;

if (NORMALIZE==1) begin
	reg xdummy=0,ydummy=0,zdummy=0;
	// cordic gain= 1/0.6072529350088812561694
	// (1./2**1)+(1./2**3)-(1./2**6)-(1./2**9)-(1./2**12)+(1./2**14)=0.607238
	// (1./2**1)+(1./2**3)-(1./2**6)-(1./2**9)-(1./2**12)+(1./2**14)+(1./2**16)-(1./2**19)+(1./2**20)-(1./2**23)-(1./2**25)=0.6072529256
	always @(posedge clk) begin
		xsum0<=(xg>>>1)+(xg>>>3);
		xsum1<=(xg>>>6)+(xg>>>9);
		xsum2<=(xg>>>12)-(xg>>>14);//+(xg>>>16)-(xg>>>19)+(xg>>>20)-(xg>>>23)-(xg>>>25);
		xsum2_d<=xsum2;
		xsum3<=xsum0-xsum1;
		{xdummy,xout_r}<=xsum3-xsum2_d;
		ysum0<=(yg>>>1)+(yg>>>3);
		ysum1<=(yg>>>6)+(yg>>>9);
		ysum2<=(yg>>>12)-(yg>>>14);//+(yg>>>16)-(yg>>>19)+(yg>>>20)-(yg>>>23)-(yg>>>25);
		ysum2_d<=ysum2;
		ysum3<=ysum0-ysum1;
		{ydummy,yout_r}<=ysum3-ysum2_d;
		//			{ydummy,yout_r}<=(yg>>>1)+(yg>>>3)-(yg>>>6)-(yg>>>9)-(yg>>>12)+(yg>>>14);//+(yg>>>16)-(yg>>>19)+(yg>>>20)-(yg>>>23)-(yg>>>25);
		//			{ydummy,yout_r}<=(yg>>>1)+(yg>>>3)-(yg>>>6)-(yg>>>9)-(yg>>>12)+(yg>>>14);//+(yg>>>16)-(yg>>>19)+(yg>>>20)-(yg>>>23)-(yg>>>25);
		//			{ydummy,yout_r}<=(yg>>>1)+(yg>>>3)-(yg>>>6)-(yg>>>9)-(yg>>>12)+(yg>>>14);//+(yg>>>16)-(yg>>>19)+(yg>>>20)-(yg>>>23)-(yg>>>25);
		zg0 <= zg;
		zg1 <= zg0;
		phaseout_r<= zg1;
		errorr<=(xdummy^xout_r[WIDTH-1])|(ydummy^yout[WIDTH-1]);
	end
	// 0x9b74eda8/(2.0**32)=0.607252934947
	//wire signed [WIDTHP1+32-1:0] xmulti=xg*32'h9b74eda8;
	//wire signed [WIDTHP1+32-1:0] ymulti=yg*32'h9b74eda8;
	//assign {xdummy,xout}=xmulti[WIDTHP1+32-2:32];
	//assign {ydummy,yout}=ymulti[WIDTHP1+32-2:32];
	// (int(0.6072529350088812561694*2**17))/2.0**17=0.6072463989257812
	/*		wire signed [17:0] gain=18'h136e9;
	wire signed [WIDTHP1+18-1:0] xmulti=xg*gain;
	wire signed [WIDTHP1+18-1:0] ymulti=yg*gain;
	*/		//assign {xdummy,xout=xmulti[WIDTHP1+18-1:17];
	//assign {ydummy,yout=ymulti[WIDTHP1+18-1:17];

end
else begin
	always @(posedge clk) begin
		xout_r<=xg[WIDTH-1:0];
		yout_r<=yg[WIDTH-1:0];
		phaseout_r <= zg;
		errorr<=1'b0;
	end
end
endgenerate
assign xout=xout_r;
assign yout=yout_r;
assign phaseout=phaseout_r;
wire signed [WIDTH-1:0] xout1=xout;
wire signed [WIDTH-1:0] yout1=yout;
wire [ZWIDTH-1:0] phaseout1=phaseout;

localparam BUFINDELAY=(BUFIN==0)? 0 : 1 ;
localparam NORMALIZEDDELAY=(NORMALIZE==0)? 1:2;
reg_delay1 #(.DW(GW), .LEN(NSTAGE+BUFINDELAY+NORMALIZEDDELAY)) reg_delay(.clk(clk), .gate(1'b1), .din(gin),   .dout(gout),.reset(1'b0));
endmodule
