module dcrej1(input [0:0] clk
,input signed [NCHAN*WIDTH-1:0] din
,output signed [NCHAN*WIDTH-1:0] dout
,input [0:0] reset
);
parameter NCHAN=8;
parameter WIDTH=16;
parameter WINTE=28;

genvar iadc;
generate for (iadc=0;iadc<NCHAN;iadc=iadc+1) begin: dcrej
	wire signed [WIDTH-1:0] xiout;
	wire signed [WIDTH-1:0] xi=din[(iadc+1)*WIDTH-1:iadc*WIDTH];
	dcrej #(.WIDTH(WIDTH),.WINTE(WINTE)) dcrej(.clk(clk),.din(xi),.dout(xiout),.reset(reset));
	assign dout[(iadc+1)*WIDTH-1:iadc*WIDTH]=xiout;
end
endgenerate
endmodule
