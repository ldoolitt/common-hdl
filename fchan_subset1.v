`timescale 1ns / 1ns

// Name: Channel Subset
//% Runtime-configurable selection of which data to keep within each block
//% Typically used on the input to a waveform memory
module fchan_subset(clk,
	keep,
	i_data, i_gate,reset,
	o_data, o_gate,
	time_err
	,currentkeep
);
parameter KEEP_LSB=0;
parameter i_dw=20;
parameter o_dw=20;
parameter len=16;



input clk;
input [len-1:0] keep;
output [len-1:0] currentkeep;
input signed [i_dw-1:0] i_data;  input i_gate, reset;
output signed [o_dw-1:0] o_data;  output o_gate;
output time_err;


// Reverse bit order of mask
//   We use historical code in fchan_subset that defines its keep input in
//   the left-to-right sense.  But for ease of documentation and consistency
//   with banyan switch code, our keep input has its bits counted from
//   right to left:  lsb is bit 0, corresponds to channel 0.
wire [len-1:0] keep_use;
reg [len-1:0] keep_use_r;
genvar ix;
generate
	if (KEEP_LSB==1)
		for (ix=0; ix<len; ix=ix+1) assign keep_use[ix] = keep[len-1-ix];
	else
		for (ix=0; ix<len; ix=ix+1) assign keep_use[ix] = keep[ix];
endgenerate

reg [len-1:0] live=0;
reg reset_d=0;

always @(posedge clk) begin
	reset_d<=reset;
	if (reset_d) begin
		live<=keep_use;
		keep_use_r<=keep_use;
	end
	else if (i_gate)
		live<={live[len-2:0],live[len-1]};
end

assign o_data = i_data;
assign o_gate = i_gate & live[len-1];
assign currentkeep=keep_use_r;
endmodule
