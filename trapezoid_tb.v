`timescale 1ns / 1ns

module trapezoid_tb;

reg clk;
reg reset;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("trapezoid.vcd");
		$dumpvars(5,trapezoid_tb);
	end
	reset=0;
	for (cc=0; cc<250; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
	$finish();
end

// Values for APEX laser output documented in trapezoid.v
reg [19:0] phase_step_h=9986;
reg [11:0] phase_step_l=1794;
reg [11:0] modulo=1;

wire [15:0] trap0, trap1;
trapezoid dut(.clk(clk), .reset(reset),
	.trap0(trap0), .trap1(trap1), .enable(1'b1),
	.phase_step_h(phase_step_h), .phase_step_l(phase_step_l), .modulo(modulo)
);

always @(negedge clk) if (cc>10) begin
	$display("%d %d",cc,trap0);
end

endmodule
