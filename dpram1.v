`timescale 1ns / 1ns
module dpram1(
	clka, clkb,
	addra, douta, dina, wena,
	addrb, doutb
);
parameter aw=8;
parameter dw=8;
parameter sz=(32'b1<<aw)-1;
	input clka, clkb, wena;
	input [aw-1:0] addra, addrb;
	input [dw-1:0] dina;
	output [dw-1:0] douta, doutb;

reg [dw-1:0] mem[sz:0];
reg [aw-1:0] ala=0, alb=0;

integer k=0;
initial
begin
	for (k=0;k<sz+1;k=k+1)
	begin
		mem[k]=0;
	end
end

assign douta = mem[addra];
assign doutb = mem[addrb];
always @(posedge clka) begin
	if (wena) mem[addra]<=dina;
end

endmodule
