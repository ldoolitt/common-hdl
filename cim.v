`timescale 1ns / 1ns

// Cascaded Integrator and Mixer
module cim(input clk
,input reset
,input [NCHAN*ADCWIDTH-1:0] adcin
,input [NCHAN*LOWIDTH-1:0] loin
,output [NCHAN*INTEWIDTH-1:0] inte
);

function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
parameter NCHAN=4;
parameter ADCWIDTH=16;
parameter LOWIDTH=18;
parameter CIC_ORDER=2;
parameter CIC_PERIOD=132;
parameter SAMPLEPERMAX=32;
parameter NORMALIZE=2;
parameter INTEADD=0;
localparam CICADD=clog2(CIC_PERIOD**CIC_ORDER);
localparam SAMPADD=clog2(SAMPLEPERMAX**CIC_ORDER);
parameter RWI=20;
wire [15:0] rwi=RWI;
localparam INTEWIDTH=RWI+SAMPADD+INTEADD;
localparam DAVR=RWI-ADCWIDTH;
localparam RWISAMP=RWI+SAMPADD;
localparam MSBGUARD=NORMALIZE==2 ? 1 : 0;


genvar iadc;
generate for (iadc=0;iadc<NCHAN;iadc=iadc+1) begin: eachadc
	wire signed [LOWIDTH-1:0] loin_iadc=$signed(loin[(iadc+1)*LOWIDTH-1:iadc*LOWIDTH]);
	wire signed [ADCWIDTH-1:0] adcf=$signed(adcin[(iadc+1)*ADCWIDTH-1:iadc*ADCWIDTH]);
	wire signed [RWI+MSBGUARD-1+1:0] mixout_w;
	reg signed [RWI+MSBGUARD-1+1:0] mixout=0;
	reg signed [RWI+MSBGUARD-1+1:0] mixout_test={1'b0,{RWI-1+MSBGUARD{1'b1}}};
//	reg signed [RWI+MSBGUARD-1:0] mixout_test={{RWI-1+MSBGUARD{1'b1}},1'b0};
//	reg signed [RWI+MSBGUARD-1:0] mixout_test=23432;
	mixer #(.dwi(ADCWIDTH),.davr(DAVR),.dwlo(LOWIDTH),.NORMALIZE(NORMALIZE))
	mixer(.clk(clk), .adcf(adcf), .mult(loin_iadc), .mixout(mixout_w));
	always @(posedge clk) begin
		mixout <= reset ? 0 : mixout_w;
	end
	wire signed [INTEWIDTH-1:0] inte_iadc;
	double_inte1 #(.DWI(RWI+MSBGUARD+1),.DWO(INTEWIDTH))
	doubleinte (.clk(clk), .in(mixout), .out(inte_iadc),.reset(reset));
	assign inte[(iadc+1)*INTEWIDTH-1:iadc*INTEWIDTH]=inte_iadc;
end
endgenerate
endmodule
