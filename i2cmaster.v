module i2cmaster#(parameter MAXNACK=4,parameter ACKWIDTH=8
,localparam DWIDTH=MAXNACK*ACKWIDTH
)
(input clk
,input sdarx
,output sdatx
,output sdaasrx
,output scl
,input [3:0] nack
//,input [15:0] r1w0
,input stopbit
,input start
,input [MAXNACK*ACKWIDTH-1:0] datatx
,output [MAXNACK*ACKWIDTH-1:0] datarx
,input [31:0] clk4ratio
,output rxvalid
,input rst
,output busy
,output resetdone
,output [DWIDTH-1:0] datatx_run
,output [3:0] nack_run
,output stopbit_run
,output [1:0] dbclkstate
,output [2:0] dbstate
,output [2:0] dbnext
,output [15:0] dbdatacnt
,output [15:0] dbackcnt
,output dbtick1
,output dbtick0
,output dbidledone
,output dbdatadone
,output dbstopdone
,output [31:0] dbclkcnt
);
reg start_r=0;
reg stopbit_r=0;
reg [3:0] nack_r=0;
always @(posedge clk) begin
	start_r<=start;
end
reg [DWIDTH-1:0] datatx_r=0;
wire r1w0=datatx_r[DWIDTH-ACKWIDTH];
//reg [MAXNACK-1:0] r1w0sr=0;//datatx_r[DWIDTH-ACKWIDTH];
wire [1:0] clkstate;
wire stop;
reg tx1rx0;
reg txbit=0;
i2cclk i2cclk(.clk(clk),.clk4ratio(clk4ratio),.start(start_r),.stop(stop),.state(clkstate),.tick0(tick0),.tick1(tick1),.dbclkcnt(dbclkcnt));
localparam IDLE=3'h0;
localparam LOAD=3'h1;
localparam START=3'h2;
localparam DATA=3'h3;
localparam ACK=3'h4;
localparam STOP=3'h5;
localparam END=3'h6;
reg [2:0] state=IDLE;
reg [2:0] next=IDLE;
reg busy_r=0;
//reg reset=0;
reg resetdone_r=0;
assign resetdone=resetdone_r;
//	reset<=rst;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		state<=IDLE;
		resetdone_r<=1'b0;
	end
	else begin
		state<=next;
		resetdone_r<=1'b1;
	end
end
reg scl_r=0;
reg sdatx_r=0;
reg [15:0] datacnt=0;
reg [15:0] ackcnt=0;
reg datadone=0;
reg ackdone=0;

reg [DWIDTH-1:0] datatxsr=0;
reg [DWIDTH-1:0] datarxsr=0;
wire cycle=(clkstate==3 & tick1) ;
reg cycle_d=0;
always @(*) begin
	if (rst) begin
		next=IDLE;
	end
	else begin
		case (state)
			IDLE: next= (start_r) ? LOAD : IDLE;
			LOAD: next= cycle_d ? START : LOAD;
			START: next =(cycle_d)  ? DATA : START;
			DATA: next= (cycle_d && (datacnt==ACKWIDTH)) ? ACK : DATA;
			ACK:  next= (cycle_d) ? (ackcnt==nack_r) ? STOP : DATA : ACK;
			STOP: next =  (cycle_d) ? IDLE : STOP;
		endcase
	end
end
assign datatx_run=busy_r ? datatx_r : 0;
assign nack_run=busy_r ? nack_r : 0;
assign stopbit_run=busy_r ? stopbit_r : 0; 
assign stop=~busy_r;//stopdone;//datacnt==DWIDTH+3;
reg [DWIDTH-1:0] datarx_r=0;
reg ack=0;
reg acktx=0;//
reg rxvalid_r=0;
reg rxvalid_rd=0;
always @(posedge clk) begin
	cycle_d<=cycle;
	if (rst) begin
		sdatx_r<=1'b1;
		scl_r<=1'b1;
		tx1rx0<=1'b1;
		datacnt<=0;
		ackcnt<=0;
		rxvalid_r<= 1'b0;
	end
	else begin
		case (next)
			IDLE: begin
				sdatx_r<=1'b1;
				scl_r<=1'b1;
				txbit<=1'b1;
				tx1rx0<=1'b1;
				datacnt<=0;
				ackcnt<=0;
			end
			LOAD: begin
				nack_r<=nack;
				datatx_r<=datatx;
				stopbit_r<=stopbit;
				busy_r<=1'b1;
				rxvalid_r<= 1'b0;
			end
			START: begin
				datatxsr<=datatx_r;
				datarxsr<=0;
				tx1rx0<=1'b1;
				case (clkstate)
					0: if (tick0) begin sdatx_r<=1'b1; end
					1: if (tick0) begin scl_r<=1'b1; end
					2: if (tick0) begin sdatx_r<=1'b0; end
					3: begin
						if (tick0) begin scl_r<=1'b0;txbit<=tx1rx0; end
						if (tick1) begin
							datacnt<=0;
						end
					end
				endcase
			end
			DATA: begin
				txbit<=tx1rx0;
				ack<=0;
				case (clkstate)
					0: if (tick0) begin sdatx_r<=datatxsr[DWIDTH-1]; datatxsr<={datatxsr[DWIDTH-2:0],1'b0}; end
					1: if (tick0) begin scl_r<=1'b1; end
					//2: begin datacnt<=datacnt+1; end
					2: if (tick0) begin datarxsr<={datarxsr[DWIDTH-2:0],sdarx}; end
					3: begin
						if (tick0) scl_r<=1'b0;
						if (tick1) begin
							datacnt<=datacnt+1;
						end
					end
				endcase
			end
			ACK:  begin
				txbit<=~tx1rx0;
				case (clkstate)
					0: if (tick0&~tx1rx0) begin sdatx_r<=(ackcnt==nack_r-1);end
					1: if (tick0) begin scl_r<=1'b1; end
					2: if (tick0&tx1rx0) begin ack<=~sdarx; end
					3:begin
						if (tick0) scl_r<=1'b0;
						if (tick1) begin
							datacnt<=0;
							ackcnt<=ackcnt+1'b1;
							//acktx<=(ackcnt==(nack_r-1));
							tx1rx0<=~r1w0;
						end
					end
				endcase
			end
			STOP: begin
				ack<=0;
				txbit<=1'b1;
				tx1rx0<=1'b1;
				case (clkstate)
					0: if (tick0) begin sdatx_r<= 1'b0; end
					1: if (tick0) begin scl_r<=stopbit_r ? 1'b1 : 1'b0; end
					2: if (tick0) begin sdatx_r<= stopbit_r ? 1'b1 : 1'b0; end
					3: begin
						if (tick1) begin
							datacnt<=0;
							ackcnt<=1'b0;
							busy_r<=1'b0;
							rxvalid_r<= 1'b1;
						end
					end
				endcase
				//	if (~tx1rx0) begin
				datarx_r<=datarxsr;
				//	end
			end
		endcase
	end
end
assign scl=scl_r;
//assign SDA= txbit ? sdatx_r : 1'bz;
//assign sdarx= txbit ? sdatx_r : SDA;
//IOBUF sdaiobuf (.IO(SDA),.I(sdatx_r),.O(sdarx),.T(~txbit));
reg [DWIDTH-1:0] datarx_d=0;
reg [DWIDTH-1:0] datarx_d2=0;
always @(posedge clk) begin
	datarx_d<=datarx_r;
	if (rxvalid_r) begin
		datarx_d2<=datarx_d;
	end
	else
		datarx_d2<=0;
	rxvalid_rd<=rxvalid_r;
end
assign sdatx=sdatx_r;
assign sdaasrx=~txbit;
assign datarx=datarx_d2;
assign rxvalid=rxvalid_rd;
assign busy=start|start_r|busy_r;
assign dbclkstate=clkstate;
assign dbstate=state;
assign dbnext=next;
assign dbdatacnt=datacnt;
assign dbackcnt=ackcnt;
assign dbtick1=tick1;
assign dbtick0=tick0;
endmodule
