`timescale 1ns / 1ns

module simple_clip(
	input clk,
	input signed [16:0] d_in,
	input [14:0] clipv,
	output signed [15:0] d_out,
	output clipped  // goes to a red light?  Probably with some latching.
);

// First pipeline step
reg cmp_hi=0, cmp_lo=0;
reg signed [16:0] d_hold=0;
wire signed [16:0] clipv_hi={2'b00,clipv};
wire signed [16:0] clipv_lo={2'b11,~clipv};
always @(posedge clk) begin
	cmp_hi <= d_in > clipv_hi;
	cmp_lo <= d_in < clipv_lo;
	d_hold <= d_in;
end

// Second pipeline step
reg flag=0;
reg signed [15:0] result=0;
always @(posedge clk) begin
	result <= cmp_hi ? clipv : cmp_lo ? (-clipv) : d_hold;
	flag <= cmp_hi | cmp_lo;
end

assign d_out = result;
assign clipped = flag;

endmodule
