`timescale 1ns / 1ns
module iloop #(parameter WIN=16
,parameter WKI=16
,parameter PKI=7
,parameter WINTE=48
,parameter WOUT=16)
(input clk
,input reset
,input signed [WIN-1:0] errin
,input [WKI-1:0] ki
,input strobe_in
,input reverse
,output strobe_out
,output signed [WIN+WKI-PKI+WINTE-1:0] inte
,output [WOUT-1:0] ctrl
,input [WOUT-1:0] staticctrl
,input openloopstatic
);


reg signed [WKI:0] ki_s=0;
reg signed [WIN+WKI-1:0] mult_r=0;

reg signed [WIN+WKI+WINTE:0] integrator_p=0;
wire signed [WIN+WKI+WINTE-1:0] integrator;
wire signed [WOUT+PKI-1:0] ctrl_calc;
sat #(.WIN(WIN+WKI+WINTE+1),.WOUT(WIN+WKI+WINTE)) integratorsat(.din(integrator_p),.dout(integrator));
sat #(.WIN(WIN+WKI+WINTE),.WOUT(WOUT+PKI)) ctrlsat(.din(integrator),.dout(ctrl_calc));

reg str_1=0,str_2=0,str_3=0;
wire [WIN+WKI+WINTE:0] integrator_s1;
wire [WIN+WKI+WINTE:0] mult_r_s1;
sext #(.WIN(WIN+WKI+WINTE),.WOUT(WIN+WKI+WINTE+1)) sextintegrator(.din(integrator),.dout(integrator_s1));
sext #(.WIN(WIN+WKI),.WOUT(WIN+WKI+WINTE+1)) sextmultr(.din(mult_r),.dout(mult_r_s1));

always @(posedge clk) begin
	ki_s <= reverse ? $signed(-{1'b0,ki}) : $signed({1'b0,ki}) ;
end
always @(posedge clk or posedge reset) begin
	if (reset) begin
		integrator_p <= 0;
		mult_r <= 0;
		str_1 <= 1'b0;
		str_2 <= 1'b0;
		str_3 <= 1'b0;
	end
	else begin
		mult_r <= errin * ki_s;
		str_1 <= strobe_in;
		str_2 <= str_1;
		str_3 <= str_2;
		if (str_1) begin
			integrator_p <= integrator_s1 + mult_r_s1 ;
		end
	end
end

assign strobe_out =str_2;
assign inte=integrator[WIN+WKI+WINTE-1:PKI];
assign ctrl = openloopstatic ? staticctrl : ctrl_calc[WOUT+PKI-1:PKI];
endmodule
