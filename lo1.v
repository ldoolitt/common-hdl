`timescale 1ns / 1ns
module lo1(input clk
//,input signed [ZWIDTHP1-1:0] phsteph_offset_use
,input [PHASE1WIDTH-1:0] phase_h
,input [ZWIDTHP1-1:0] phase_offset
,input [(NRIDER+1)*WIDTHP1-1:0] xlo
// should only use WIDTH bit per, but for cordicg easy to pickup, use WIDTHP1, so take care of 1 extra guard bit from upper layer
,input [(NRIDER+1)*WIDTHP1-1:0] ylo
,output signed [(NRIDER+1)*WIDTH-1:0] cos
,output signed [(NRIDER+1)*WIDTH-1:0] sin
//,output signed [(NRIDER+1)*ZWIDTHP1-1:0] phaselo
,input [RATIOWIDTH-1:0] ratio
);
parameter NRIDER=0;
parameter WIDTH=20;
localparam ZWIDTH=WIDTH+1;
localparam ZWIDTHP1=ZWIDTH+1;
parameter PHASE1WIDTH=31;
parameter PHSTEPH=16268815;
parameter PHSTEPL=2108;
parameter MODULO=4;
parameter NSTAGE=WIDTH;
parameter RATIOWIDTH=9;
localparam WIDTHP1=WIDTH+1;

wire [0:0] errorlo;
reg [ZWIDTHP1-1:0] phase_hr=0;
reg [RATIOWIDTH-1:0] ratior=0;
//wire [RATIOWIDTH-1:0] ratio=RATIO;
wire [ZWIDTHP1+RATIOWIDTH-1:0] multi;
reg [ZWIDTHP1+RATIOWIDTH-1:0] multir=0;
reg [ZWIDTHP1-1:0] multir1=0;
reg [ZWIDTHP1-1:0] phaselobase=0;
wire multisign=multi[ZWIDTHP1+RATIOWIDTH-1];
always @(posedge clk) begin
	phase_hr<=phase_h[PHASE1WIDTH-1:PHASE1WIDTH-ZWIDTHP1];  // we have lots of multiplier? or space? can be replaced by multi_trad.;
	ratior<=ratio;
	multir<=multi;
	multir1<=multir[ZWIDTHP1-1:0];//[PHASE1WIDTH-1:PHASE1WIDTH-ZWIDTHP1];
	phaselobase <= multir1+phase_offset;
end

mult_trad1 #(.WX(RATIOWIDTH),.WY(ZWIDTHP1))
mult_trad1 (.clk(clk),.x(ratior),.y(phase_hr),.z());
assign multi=(ratior)*(phase_hr);

wire [(NRIDER+1)*ZWIDTHP1-1:0] phasein={(NRIDER+1){phaselobase}};//+phsteph_offset_use}};
wire [(NRIDER+1)*(WIDTH+1)-1:0] xout,yout;
wire [(NRIDER+1)*(ZWIDTH+1)-1:0] zout;
wire cordicgerror;
cordicg1 #(.WIDTH(WIDTHP1),.NSTAGE(NSTAGE),.NORMALIZE(0),.NRIDER(NRIDER))
cordicg1(.clk(clk),.opin(1'b0),.phasein(phasein),.xin(xlo),.yin(ylo)//{WIDTHP1{1'b0}})
,.xout(xout),.yout(yout),.phaseout(zout)
,.error(cordicgerror)
);
//assign phaselo=phasein;
genvar irider;
generate for (irider=0; irider<NRIDER+1; irider=irider+1) begin: lo
assign cos[(irider+1)*WIDTH-1:irider*WIDTH]=xout[(irider+1)*(WIDTH+1)-1:irider*(WIDTH+1)+1]+xout[irider*(WIDTH+1)];
assign sin[(irider+1)*WIDTH-1:irider*WIDTH]=yout[(irider+1)*(WIDTH+1)-1:irider*(WIDTH+1)+1]+yout[irider*(WIDTH+1)];
end
endgenerate
assign errorlo=zout[ZWIDTH-1:1]|(~|zout[ZWIDTH-1:1])|cordicgerror;
assign error = |errorlo;
endmodule
/*cordicg1 #(.WIDTH(WIDTHP1),.NSTAGE(NSTAGE),.NORMALIZE(1),.NRIDER(1))
kpkirot(.clk(clk),.opin(1'b0),.phasein({kpkiph,1'b0,kpkiph,1'b0}),.xin({2*WIDTHP1{1'b0}}),.yin({-kp,1'b0,-ki,1'b0})//{WIDTHP1{1'b0}})
,.xout({kprx_w,kirx_w}),.yout({kpry_w,kiry_w}),.phaseout({kpph,kiph})
,.error(kpkierr)
);
*/
