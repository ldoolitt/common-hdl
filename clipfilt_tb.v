`timescale 1ns / 1ns
`include "constants.vams"
`include "freq.vh"

module clipfilt_tb;

reg clk;
integer cc, errors;
integer debug=1;
reg trace;
initial begin
	errors = 0;
	trace = $test$plusargs("trace");
	for (cc=0; cc<60; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
	$display("%d errors  %s", errors, errors>0 ? "FAIL" : "PASS");
	$finish();
end

integer ccr;
real volt;
real theta = `M_TWO_PI*`RF_NUM/`COHERENT_DEN;
reg signed [15:0] d_in=0, temp;
always @(posedge clk) begin
	if (cc%50==10) begin  // impulse response
		d_in <= 30000;
	end else if (cc>20 && cc<40+`COHERENT_DEN) begin
		ccr  = cc%`COHERENT_DEN;
		volt = $sin(theta*ccr);
		temp = $floor(30000*volt+0.5);
		d_in <= temp;
	end else begin
		d_in <= 0;
	end
end

wire signed [15:0] d_out;
clipfilt mut(
	.clk(clk), .d_in(d_in), .d_out(d_out)
);

integer cc2;
parameter pipe_len = 1;
real th2, sum_in_cos, sum_in_sin, sum_out_cos, sum_out_sin;
real amp_in, amp_out, gain, gain_want;
real ph_in, ph_out, ph_diff, ph_want;
always @(negedge clk) begin
	if (trace) $display("%d %d", d_in, d_out);
	cc2 = cc%`COHERENT_DEN;
	th2 = ccr*theta;
	if (cc==30) begin
		sum_in_sin = 0;
		sum_in_cos = 0;
		sum_out_sin = 0;
		sum_out_cos = 0;
	end else begin
		sum_in_sin = sum_in_sin + d_in * $sin(th2);
		sum_in_cos = sum_in_cos + d_in * $cos(th2);
		sum_out_sin = sum_out_sin + d_out * $sin(th2);
		sum_out_cos = sum_out_cos + d_out * $cos(th2);
	end
	if (cc==30+`COHERENT_DEN) begin
		ph_in = $atan2(sum_in_sin,sum_in_cos);
		ph_out = $atan2(sum_out_sin, sum_out_cos);
		amp_in = $sqrt(sum_in_sin*sum_in_sin+sum_in_cos*sum_in_cos)*2/`COHERENT_DEN;
		amp_out = $sqrt(sum_out_sin*sum_out_sin+sum_out_cos*sum_out_cos)*2/`COHERENT_DEN;
		if (trace) $display("# sums %6.0f %6.0f   %6.0f %6.0f",
			sum_in_cos, sum_in_sin,
			sum_out_cos, sum_out_sin);
		gain = amp_out/amp_in;
		ph_diff =  ph_out - ph_in;
		if (ph_diff<0) ph_diff = ph_diff + `M_TWO_PI;
// clipfilt.m reports gain of 0.8732, phase shift -36.98 degrees = -0.6454 radians
		gain_want = 0.8732;
		ph_want = theta*pipe_len+0.6454;

		$display("clipfilt testing at carrier %d/%d * clk",`RF_NUM,`COHERENT_DEN);
		$display("filter gain %6.4f (want %6.4f)",gain, gain_want);
		if ($abs(gain/gain_want-1.0)>0.0005) errors = errors+1;

		$display("filter phase shift %6.4f radians (want %6.4f)", ph_diff, ph_want);
		if ($abs(ph_diff-ph_want)>0.0005) errors = errors+1;
	end
end

endmodule
