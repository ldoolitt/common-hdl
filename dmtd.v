module dmtd #(parameter SIM=0,parameter RISING=1,parameter FOFFSETWIDTH=14)
(input clkdmtd
,input clka
,input clkb
,input rst
//,input [4:0] navr
//,input [4:0] refcnt
,input [31:0] stableval
,input [4:0] offsetwidth
//,output [31:0] phdiff
//,output [31:0] phdiffmid
,output [31:0] freqa
,output [31:0] freqb
,output [31:0] freqa1
,output pvalid
//,output [31:0] phdiffavr
//,output [31:0] phdiffmidavr
//,output stb_phdiffavr
,output [31:0] dbastable
,output [31:0] dbbstable
,output [31:0] dbphdiff
,output [31:0] dbphdiffmid
,output [31:0] freqerr
,output stb_freqerr
,output stb_phaseab
,output [31:0] phaseab
//,output [31:0] dbacc1
//,output [31:0] dbacc1mid
,output dbpvalid
,output signed [31:0] dbafreq
,output signed [31:0] dbbfreq
,output dbsclka
,output dbsclkb
,output [31:0] dbclkdmtdcnt
,output [31:0] dbsclkacnt
,output [31:0] dbsclkbcnt
,output dbavalid
,output dbbvalid
,output [1:0] dbstate
,output [1:0] dbnext
,output dbstable_sclka
,output dbstable_sclkb
,output [31:0] dbfreecnta
,output [31:0] dbfreecntb
,output dbafreq01
,output [31:0] dbfreqint
,output [31:0] dbfracerr
);
reg [31:0] freecnta=0;
reg [32+FOFFSETWIDTH-1:0] cntaoffset_r=0;
wire [31:0] cntaoffset=cntaoffset_r[32+FOFFSETWIDTH-1:FOFFSETWIDTH];
always @(posedge clka or posedge rst) begin
	freecnta<=rst ? 0 : freecnta+1;
	cntaoffset_r<=rst ? 0 : cntaoffset_r + (1<<FOFFSETWIDTH) + 1;
end
reg [31:0] freecntb=0;
always @(posedge clkb or posedge rst) begin
	freecntb<=rst ? 0 : freecntb+1;
end
assign dbfreecnta=freecnta;
assign dbfreecntb=freecntb;
wire sclka;
wire sclkb;
assign dbsclka=sclka;
assign dbsclkb=sclkb;
wire arisingstable;
wire brisingstable;
wire afalingstable;
wire bfalingstable;
async clkaasig(.clk(clkdmtd),.asig(clka),.ssig(),.ssig_val(sclka));
async clkbasig(.clk(clkdmtd),.asig(clkb),.ssig(),.ssig_val(sclkb));

reg [31:0] clkdmtdcnt=0;
reg sclka_d=0;
reg [31:0] sclkacnt=0;
reg stable_sclka=0;
reg stable_sclka_d=0;
wire firstaglitch=(sclkacnt==0)&(sclka!=sclka_d);
always @(posedge clkdmtd) begin
	sclka_d<=sclka;
	sclkacnt<=(sclka!=sclka_d) ? stableval-1 : ~|sclkacnt ? 0 : sclkacnt-1;
	if (sclkacnt==0) begin
		stable_sclka<=sclka_d;
	end
	stable_sclka_d<=stable_sclka;
end
assign afalingstable=~stable_sclka&stable_sclka_d;
assign arisingstable=stable_sclka&~stable_sclka_d;
reg sclkb_d=0;
reg [31:0] sclkbcnt=0;
reg stable_sclkb=0;
reg stable_sclkb_d=0;
wire firstbglitch=(sclkbcnt==0)&(sclkb!=sclkb_d);
always @(posedge clkdmtd) begin
	sclkb_d<=sclkb;
	sclkbcnt<=(sclkb!=sclkb_d) ? stableval-1 : ~|sclkbcnt ? 0 : sclkbcnt-1;
	if (sclkbcnt==0)
		stable_sclkb<=sclkb_d;
	stable_sclkb_d<=stable_sclkb;

end
assign bfalingstable=~stable_sclkb&stable_sclkb_d;
assign brisingstable=stable_sclkb&~stable_sclkb_d;
assign dbsclkacnt=sclkacnt;
assign dbsclkbcnt=sclkbcnt;
assign dbstable_sclka=stable_sclka;
assign dbstable_sclkb=stable_sclkb;
assign dbclkdmtdcnt=clkdmtdcnt;
reg signed [31:0] astable=0;
reg signed [31:0] bstable=0;
reg signed [31:0] aglitch=0;
reg signed [31:0] bglitch=0;
reg signed [31:0] astable_d=0;
reg signed [31:0] bstable_d=0;
assign dbastable=astable;
assign dbbstable=bstable;
wire signed [31:0] afreq=astable-astable_d;
wire signed [31:0] bfreq=bstable-bstable_d;
wire signed [31:0] afreq_n=astable_d-astable;
wire signed [31:0] bfreq_n=bstable_d-bstable;
assign dbafreq=afreq;
assign dbbfreq=bfreq;
assign freqa=afreq;
assign freqb=bfreq;
reg avalid=0;
reg bvalid=0;
assign dbavalid=avalid;
assign dbbvalid=bvalid;
wire aedge=RISING ? arisingstable : afalingstable;
wire bedge=RISING ? brisingstable : bfalingstable;
always @(posedge clkdmtd) begin
	clkdmtdcnt<=rst ? 0  : clkdmtdcnt+1;
	avalid<=aedge;
	bvalid<=bedge;
	if (aedge) begin
		astable<=clkdmtdcnt;
		astable_d<=astable;
	end
	if (bedge) begin
		bstable<=clkdmtdcnt;
		bstable_d<=bstable;
	end
	if (firstaglitch) begin
		aglitch<=clkdmtdcnt;
	end
	if (firstbglitch) begin
		bglitch<=clkdmtdcnt;
	end
end
wire [32:0] amid2=$unsigned(astable)+$unsigned(aglitch);
wire [31:0] amid=amid2[32:1];
wire [32:0] bmid2=$unsigned(bstable)+$unsigned(bglitch);
wire [31:0] bmid=bmid2[32:1];
reg signed [31:0] phdiff_r=0;
reg signed [31:0] phdiffmid_r=0;
assign dbphdiff=phdiff_r;
assign dbphdiffmid=phdiffmid_r;
reg pvalidab=0;
localparam WAITAB=2'h0;
localparam WAITA=2'h1;
localparam WAITB=2'h2;
reg [1:0] state=WAITAB;
reg [1:0] next=WAITAB;
assign dbstate=state;
assign dbnext=next;
always @(posedge clkdmtd) begin
	if (rst) begin
		state<=WAITAB;
	end
	else begin
		state<=next;
	end
end
always @(*) begin
	if (rst) begin
		next=WAITAB;
	end
	else begin
		case (state)
			WAITAB: next= (avalid & bvalid) ?  WAITAB : avalid ? WAITB : bvalid ? WAITA : WAITAB;
			WAITA: next= pvalidab ? WAITAB : WAITA;
			WAITB: next= pvalidab ? WAITAB : WAITB;
		endcase
	end
end
reg pvalid_r=0;
assign dbpvalid=pvalid_r;
always @(posedge clkdmtd ) begin
	if (rst) begin
		phdiff_r<=0;
		phdiffmid_r<=0;
		pvalidab<=0;
	end
	else begin
		case (next)
			WAITAB: pvalidab<=avalid&bvalid;
			WAITA: pvalidab<=avalid;
			WAITB: pvalidab<=bvalid;
		endcase
		if (pvalidab) begin
			phdiff_r<=$signed(bstable)-$signed(astable);
			phdiffmid_r<=$signed(bmid)-$signed(amid);
		end
	end
	pvalid_r<=pvalidab;
end
wire [31:0] oneval=1<<(FOFFSETWIDTH+offsetwidth);
wire [31:0] offset=1<<offsetwidth;
wire [31:0] onemask=(1<<FOFFSETWIDTH+offsetwidth)-1;
reg [31:0] freecnta_x=0;
reg [31:0] freecnta_xd=0;
reg [31:0] astable_acnt=0;
reg [31:0] astable_acnt_d=0;
reg [31:0] freqdiffint=0;
reg signed [63:0] freqasum=0;
reg signed [63:0] freqasum_d=0;
reg signed [63:0] freqdifffrac=0;
reg signed [63:0] freqdifffrac_n=0;
reg signed [31:0] afreq_pr=0;
reg signed [31:0] afreq_nr=0;
reg samp=0;
reg first=1;
always @(posedge clkdmtd) begin
	samp<=~|(clkdmtdcnt&onemask);
	//freecnta_x<=cntaoffset;//freecnta;
	freecnta_x<=freecnta;
//	freqasum<=freqasum+afreq;
	if (rst) begin
		first<=1'b1;
	end
	else begin
		if (samp) begin
			first<=1'b0;
			freecnta_xd<=freecnta_x;
			freqdiffint<=first ? 0 : (freecnta_x-freecnta_xd)-oneval-offset;
		end
	end
	if (aedge) begin
		astable_acnt<=freecnta_x;
		astable_acnt_d<=astable_acnt;
//		afreq_pr<=afreq;//-oneval;
//		afreq_nr<=afreq_n;//-oneval;
//		freqasum_d<=freqasum;
		//freqdifffrac<=freqasum-freqasum_d;
		//freqdifffrac_n<=freqasum_d-freqasum;
	end
end
assign freqerr=freqdiffint;//ferr32;
assign stb_freqerr=samp;
assign stb_phaseab=pvalid_r;
assign phaseab=phdiff_r;
endmodule
