#wren 		0 				required 	out				 1 				required 		in 				1
#portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width
proc busport {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
puts $master_direction
incr $default_value 0
incr $master_width 0
incr $slave_width 0
ipx::add_bus_abstraction_port ${portname} [ipx::current_busabs]
set_property default_value $default_value [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
set_property master_presence $master_presence [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
set_property master_direction $master_direction [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
set_property master_width $master_width [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
set_property slave_presence $slave_presence [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
set_property slave_direction $slave_direction [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
set_property slave_width $slave_width [ipx::get_bus_abstraction_ports ${portname} -of_objects [ipx::current_busabs]]
}
proc portpin {portname physical_name} {
ipx::add_port_map ${portname} [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
set_property physical_name ${physical_name} [ipx::get_port_maps ${portname} -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
}

create_project vivado_project/iflocalbus iflocalbus -part xczu49dr-ffvf1760-2-e -force
add_files -norecurse ../../submodules/common-hdl/axi4_lb.v

ipx::package_project -root_dir vivado_project/iflocalbus -vendor user.org -library user -taxonomy /UserIP -import_files -set_current false -force
ipx::unload_core vivado_project/iflocalbus/component.xml
#ipx::edit_ip_in_project -upgrade true -name tmp_edit_project -directory /home/ghuang/pynqtop/zcu216/rfsoctest/boards/zcu216/ip_repo /home/ghuang/pynqtop/zcu216/rfsoctest/boards/zcu216/ip_repo/component.xml
#update_compile_order -fileset sources_1
#current_project project_1
ipx::open_ipxact_file vivado_project/iflocalbus/component.xml
#ipx::merge_project_changes hdl_parameters [ipx::current_core]
ipx::create_abstraction_definition user user iflocalbus_rtl 1.0
ipx::create_bus_definition user user iflocalbus 1.0
set_property xml_file_name vivado_project/iflocalbus/iflocalbus_rtl.xml [ipx::current_busabs]
set_property xml_file_name vivado_project/iflocalbus/iflocalbus.xml [ipx::current_busdef]
set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::current_busabs]
ipx::save_abstraction_definition [ipx::current_busabs]
ipx::save_bus_definition [ipx::current_busdef]
set_property  ip_repo_paths  vivado_project/iflocalbus [current_project]
update_ip_catalog

# {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
# busport {portname default_value master_presence master_direction master_width slave_presence slave_direction slave_width} {
busport wren 0 required out 1 required in 1
busport waddr 0 required out 32 required in 32
busport wdata 0 required out 32 required in 32
busport rden 0 required out 1 required in 1
busport rdenlast 0 required out 1 required in 1
busport rvalid 0 required in 1 required out 1
busport rvalidlast 0 required in 1 required out 1
busport raddr 0 required out 32 required in 32
busport rdata 0 required in 32 required out 32
busport clk 0 required out 1 required in 1
busport aresetn 0 required out 1 required in 1
#}
ipx::save_bus_definition [ipx::current_busdef]
set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::current_busabs]
ipx::save_abstraction_definition [ipx::current_busabs]
update_ip_catalog -rebuild -repo_path vivado_project/iflocalbus
ipx::add_bus_interface lb [ipx::current_core]

set_property abstraction_type_vlnv user:user:iflocalbus_rtl:1.0 [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
set_property bus_type_vlnv user:user:iflocalbus:1.0 [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
set_property interface_mode master [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]

portpin rdata lb_rdata
portpin raddr lb_raddr
portpin rden lb_rden
portpin rdenlast lb_rdenlast
portpin wdata lb_wdata
portpin waddr lb_waddr
portpin wren lb_wren
portpin rvalid lb_rvalid
portpin rvalidlast lb_rvalidlast
portpin clk lb_clk
portpin aresetn lb_aresetn



#ipx::add_port_map rdata [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_rdata [ipx::get_port_maps rdata -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map waddr [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_waddr [ipx::get_port_maps waddr -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map wstrb [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_wstrb [ipx::get_port_maps wstrb -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map wdata [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_wdata [ipx::get_port_maps wdata -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map wren [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_wvalid [ipx::get_port_maps wren -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map raddr [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_raddr [ipx::get_port_maps raddr -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map clk [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_clk [ipx::get_port_maps clk -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]
#ipx::add_port_map aresetn [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]
#set_property physical_name lb_aresetn [ipx::get_port_maps aresetn -of_objects [ipx::get_bus_interfaces lb -of_objects [ipx::current_core]]]

set_property interface_mode master [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
#ipx::remove_port_map CLK [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
#ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
ipx::add_bus_parameter FREQ_HZ [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]
set_property value CLK_FREQ_HZ [ipx::get_bus_parameters FREQ_HZ -of_objects [ipx::get_bus_interfaces lb_clk -of_objects [ipx::current_core]]]

#set_property core_revision 2 [ipx::current_core]
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::check_integrity [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property  ip_repo_paths  {vivado_project/iflocalbus} [current_project]
update_ip_catalog
close_project


