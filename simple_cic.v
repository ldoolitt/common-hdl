`timescale 1ns / 1ns
module simple_cic #(parameter DWIN=16
,parameter DWOUT=18
,parameter MAXCICW=4
,parameter SATOUT=0
)(input clk
,input reset
,input [WCIC-1:0] ncic
,input gin
,output gout
,input signed [DWIN-1:0] din
,output signed [DWOUT-1:0] dout
);

localparam WCIC=$clog2(MAXCICW);
reg [MAXCICW-1:0] count=0;
reg signed [DWIN+MAXCICW-1:0] inte=0;
reg signed [DWIN+MAXCICW-1:0] prev_inte=0;
reg signed [DWIN+MAXCICW-1:0] diff=0;
reg [MAXCICW-1:0] maxcic=0;
wire count0 = ~|count;
reg gout_r=0;
reg gout_r2=0;
reg [DWIN+MAXCICW-1:0] diffshift=0;
always @(posedge clk) begin
	maxcic<=((1<<ncic)-1);
	if (reset) begin
		count<=0;
		inte<=0;
		prev_inte<=0;
		diff<=0;
		gout_r<=1'b0;
	end
	else begin
		if (gin) begin
			inte <= inte + din;
			count <= count0 ? maxcic : count-1;
			if (count0) begin
				prev_inte <= inte;
				diff <= $signed(inte) - $signed(prev_inte);
				gout_r<=1'b1;
			end
			else begin
				gout_r<=1'b0;
				//diff<=0;
			end
		end
		else begin
			gout_r<=1'b0;
			//diff<=0;
		end
	end
	gout_r2<=gout_r;
	diffshift<=diff>>>ncic;
end
assign gout=gout_r2;
assign dout=diffshift[DWOUT-1:0];
endmodule
