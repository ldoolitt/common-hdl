module sext  #(
parameter WIN=16
,parameter WOUT=17)
(input signed [WIN-1:0] din
,output signed [WOUT-1:0] dout
);
// signed extension
// WIN shoujld < WOUT
localparam WSIGN=WOUT-WIN;

assign dout = $signed({{(WSIGN){din[WIN-1]}},din});
endmodule
